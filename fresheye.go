// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Package fresheye provides a way to find paronyms in Russian text.
package fresheye

const (
	defaultContextSize          = 20
	defaultSensitivityThreshold = 400
	defaultExcludeProperNames   = false
)

// BadPair is a word pair and a measure of similarity (badness) of these words.
type BadPair struct {
	Badness       float64
	First, Second WordDescriptor
}

// WordDescriptor describes the position of a word in text.
type WordDescriptor interface {
	Start() int
	End() int
}

// WordIterator returns words one by one.
type WordIterator interface {
	Next() bool
	Word() string
	Sep() string
	ParagraphStart() bool
	Descriptor() WordDescriptor
}
