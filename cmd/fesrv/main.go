// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Command fesrv provides a Fresh Eye server.
package main

import (
	"flag"
	"html/template"
	"log"
	"net/http"
	"os"
	"strconv"

	"gitlab.com/opennota/fresheye"
)

var (
	addr = flag.String("http", ":3003", "Адрес HTTP-сервера")

	tmpl = template.Must(template.New("index").Parse(indexHTML))
)

func logError(err error) {
	log.Println("ERR", err)
}

func internalError(w http.ResponseWriter, err error) {
	logError(err)
	http.Error(w, "внутренняя ошибка сервера", http.StatusInternalServerError)
}

type params struct {
	ContextSize          int
	SensitivityThreshold int
	ExcludeProperNames   bool
	Text                 string
	GTMID                string
}

func handler(w http.ResponseWriter, r *http.Request) {
	gtmID := os.Getenv("GTM_ID")

	if r.Method == "GET" {
		if err := tmpl.Execute(w, params{
			ContextSize:          20,
			SensitivityThreshold: 400,
			ExcludeProperNames:   false,
			GTMID:                gtmID,
		}); err != nil {
			logError(err)
		}
		return
	}

	err := r.ParseMultipartForm(1 * 1024 * 1024)
	if err != nil {
		internalError(w, err)
		return
	}

	contextSize, _ := strconv.Atoi(r.PostFormValue("contextSize"))
	threshold, _ := strconv.Atoi(r.PostFormValue("sensitivityThreshold"))
	excProper := r.PostFormValue("excludeProperNames") != ""
	text := r.PostFormValue("text")
	html := fresheye.ToColoredHTML(text, contextSize, threshold, excProper)

	if err := tmpl.Execute(w, struct {
		params
		Text string
		HTML template.HTML
	}{
		params{
			ContextSize:          contextSize,
			SensitivityThreshold: threshold,
			ExcludeProperNames:   excProper,
			GTMID:                gtmID,
		},
		text,
		template.HTML(html), //nolint:gosec
	}); err != nil {
		logError(err)
	}
}

func main() {
	flag.Parse()
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(*addr, nil))
}

var indexHTML = `<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <title>Свежий Взгляд Онлайн</title>
    <style>
      .container {
        padding-right: 15px;
        padding-left: 15px;
        margin-right: auto;
        margin-left: auto;
      }
      @media (min-width: 768px) {
        .container {
          width: 750px;
        }
      }
      @media (min-width: 992px) {
        .container {
          width: 970px;
        }
      }
      @media (min-width: 1200px) {
        .container {
          width: 1170px;
        }
      }
    </style>
    {{ if .GTMID }}
      <!-- Google Tag Manager -->
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','{{ .GTMID }}');</script>
      <!-- End Google Tag Manager -->
    {{ end }}
  </head>
  <body>
    {{ if .GTMID }}
      <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{ .GTMID }}"
      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->
    {{ end }}
    <div class="container">
      <form action="/" method="POST" enctype="multipart/form-data">
        <div>
          <div>
            <label for="text">Текст:</label>
          </div>
          <textarea id="text" name="text" rows="5" style="width: 100%" spellcheck="false" autofocus>
            {{- .Text -}}
          </textarea>
        </div>
        <div style="margin-top: 10px;">
          <label for="contextSize">Размер контекста (1-50):</label>
          <input id="contextSize" name="contextSize" type="text" size="4" value="{{ .ContextSize }}">
          <label for="sensitivityThreshold">Порог чувствительности (100-1000):</label>
          <input id="sensitivityThreshold" name="sensitivityThreshold" type="text" size="4"
            value="{{ .SensitivityThreshold }}">
          <input type="checkbox" id="excludeProperNames" name="excludeProperNames"
            {{ if .ExcludeProperNames}}checked{{ end }}>
          <label for="excludeProperNames">Исключить имена собственные</label>

          <button type="submit">
            Проверить
          </button>
        </div>

        <br>

        {{ if .Text }}
          {{ .HTML }}
        {{ end }}
      </form>
    </div>
  </body>
</html>`
